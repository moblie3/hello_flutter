// import 'package:flutter/material.dart';
// void main() => runApp(HelloFlutterApp());
// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//     //Scaffold widget
//     home: Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "Hello Tesla"
//         ),
//         leading: Icon(Icons.home),
//         actions: <Widget>[
//           IconButton(
//               onPressed: (){},
//               icon: Icon(Icons.refresh))
//         ],
//       ),
//       body: Center(
//         child: Text(
//           "Hello Flutter",
//         style: TextStyle(fontSize: 24),),
//       ),
//     )
//     );
//   }
//
// }
import 'package:flutter/material.dart';

void main()=> runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japaneseGreeting = "こんにちはフラッター";
String koreanGreeting = "헬로 플러터";
class _MyStatefulWidgetState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
    //Scaffold widget
    home: Scaffold(
      appBar: AppBar(
        title: Text(
          "Hello Tesla"
        ),
        leading: Icon(Icons.home),
        actions: <Widget>[
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?  spanishGreeting : japaneseGreeting;
                });
              },
              icon: Icon(Icons.sync_alt)),
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting? japaneseGreeting : koreanGreeting;
                });
              },
              icon: Icon(Icons.more_horiz)),
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting? japaneseGreeting : spanishGreeting;
                });
              },
              icon: Icon(Icons.language)),
        ],
      ),
      body: Center(
        child: Text(
          displayText,
        style: TextStyle(fontSize: 24),),
      ),
    )
    );
  }

}
